(defproject epi-clojure "0.0.1-SNAPSHOT"
  :description "A set of Clojure Katas based around the Elements of Programming Interview Questions"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [;; core language libraries
                 [org.clojure/clojure "1.10.0"]

                 ;; logging dependencies
                 [com.taoensso/timbre "4.10.0"]             ;; https://github.com/ptaoussanis/timbre native clojure logging framework
                 [expound "0.7.2"]                          ;; https://github.com/bhb/expound  ...for nice spec messages
                 ]
  :min-lein-version "2.0.0"
  :resource-paths ["config", "resources"]

  :target-path "target/%s"
  :plugins [[lein-pprint "1.1.1"]
            [lein-cloverage "1.0.13"]]

  ;; If you use HTTP/2 or ALPN, use the java-agent to pull in the correct alpn-boot dependency
  ;:java-agents [[org.mortbay.jetty.alpn/jetty-alpn-agent "2.0.5"]]
  :profiles {:dev     {:aliases      {"run-dev" ["trampoline" "run" "-m" "pedestal-tutorial.server/run-dev"]}
                       :dependencies [[io.pedestal/pedestal.service-tools "0.5.5"]
                                      [org.clojure/tools.namespace "0.2.11"]]
                       :source-paths ["dev" "src" "test"]}
             :test    [:dev {:dependencies [[org.clojure/test.check "0.10.0-alpha3"]]}]    ;; https://github.com/clojure/test.check
             :ccov    [:test {:plugins   [[lein-cloverage "1.0.13"]]                       ;; https://github.com/cloverage/cloverage (code coverage)
                              :cloverage {:test-ns-regex [#"^((?!(e2e|integration_tests)).)*$"]}}]
             :uberjar {:aot [epi-clojure.core]}}

  :repl-options {:init (do
                       ;; clojure repl goodness
                         (require '[clojure.edn :as edn])
                         (require '[clojure.java.io :as io])
                         (require '[clojure.java.javadoc :as jdoc])
                         (require '[clojure.repl :refer :all])
                         (require '[clojure.pprint :as pp])
                         (require '[clojure.inspector :as insp])
                         (require '[clojure.reflect :as reflect])
                         (require '[clojure.tools.namespace.repl :refer [refresh]])

                       ;; spec
                         (require '[clojure.spec.alpha :as s])
                         (require '[clojure.spec.gen.alpha :as gen])
                         (require '[clojure.spec.test.alpha :as stest])
                         (require '[expound.alpha :as e])

                       ;; our app namespaces we want in repl         
                         )}
  :main ^{:skip-aot true} epi-clojure.main)

