(ns epi-clojure.searching.binary
  (:gen-class))

;; ~.47 ms for 100K collection
;; ..this version much faster than mine. 
(defn bsearch [sorted-coll val]
      (loop [left 0
             right (dec (count sorted-coll))]
        (when (<= left right)
          (let [m (int (Math/floor (/ (+ left right) 2)))
                actual (nth sorted-coll m)]
            (cond
              (= actual val) m
              (< actual val) (recur (inc m) right)
              (> actual val) (recur left (dec m)))))))

