(ns epi-clojure.algorithms.minimum-spanning
  (:require [clojure.spec.alpha :as spec]
            [epi-clojure.graphs.digraph :as digraph :refer [graph->edges]]
            [epi-clojure.algorithms.union-find :as uf]
            [taoensso.timbre :as timbre
             :refer [log  trace  debug  info  warn  error  fatal  report
                     logf tracef debugf infof warnf errorf fatalf reportf
                     spy get-env]]))

(defn edges [graph]
  (->> graph
       graph->edges
       (sort-by :weight)
       (filter #(not= (:from %) (:to %)))))

;; kruskal's algorithm uses the union find structure to find the minimum spanning tree out 
;; of any one of a forest of disjoint sets.
(defn kruskal [graph]
  ;; PRE: remove any loops and redundant edges
  ;; a) sort the edges by weight low -> high
  ;; b) take the edge with the lowest weight and add it to the tree
  ;; c) keep adding until we've reached all vertices
  (let [mst (digraph/digraph)
        vertices (keys (:nodes graph))
        union-find (uf/construct-union-find
                    (count vertices)
                    (uf/->VertexStorageLayer (uf/vertex->index-map vertices)
                                             (uf/index->vertex-map vertices)))
        edge-set (edges graph)]
    (assoc mst :nodes (apply merge-with conj (for [edge edge-set :when (false? (uf/find* union-find (:from edge) (:to edge)))]
                                               (let [{:keys [from to weight]} edge]
                                                 (uf/union* union-find from to)
                                                 (:nodes (-> mst
                                                             (digraph/add-vertex from)
                                                             (digraph/add-vertex to)
                                                             (digraph/add-edge from to weight)
                                                             (digraph/add-edge to from weight)))))))))

;; most prim algorithms assume a connected graph
;; and use a Fibbonnaci heap for performance (it is O(V^2) otherwise)
(defn prim-spanning-calculation [graph])
