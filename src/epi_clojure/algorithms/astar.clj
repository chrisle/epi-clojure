(ns epi-clojure.aglorithms.a*
  "Functional implementation of A*, a best first path finding algorithm"
  (:require [clojure.math.numeric-tower :as math]))

(def world [[1   1   1   1   1]
            [999 999 999 999   1]
            [1   1   1   1   1]
            [1 999 999 999 999]
            [1   1   1   1   1]])

(defn- sqr
  "Uses the numeric tower expt to square a number"
  [x]
  (math/expt x 2))

(defn euclidean-squared-distance
  "Computes the Euclidean squared distance between two sequences"
  [a b]
  (reduce + (map (comp sqr -) a b)))

(defn euclidean-distance
  "Computes the Euclidean distance between two sequences"
  [a b]
  (math/sqrt (euclidean-squared-distance a b)))

(derive ::manhatten ::heuristic)
(derive ::diagonal ::heuristic)
(derive ::euclidian ::heuristic)

(defmulti estimate-cost :heuristic)

(defmethod estimate-cost ::manhatten 
  "Estimate the cost of a linear path from the given point {y, x} across N steps and down N steps"
  [step-estimate size y x]
  (* step-estimate
     (- (+ size size) y x 2)))

(defmethod estimate-cost ::diagonal
  "Estimate the cost of a diagonal path from the given point {y, x}"
  [step-estimate size y x]

  (let [y-cost (- size y)
        x-cost (- size x)]
    (* step-estimate
       (if (> y-cost x-cost)
         y-cost
         x-cost))))

;; TBD
(defmethod estimate-cost ::euclidian   
  "Estimate the cost of a path from the given point
{y, x} using the euclidian distance formula"
  [step-estimate size y x]
  (let [distance (euclidean-distance [y x] [size size])]))

;; (defn estimate-cost
;;   "Estimate the cost of a linear path from the given point
;; {y, x} across N steps and down N steps"
;;   [step-estimate size y x]
;;   (* step-estimate
;;      (- (+ size size) y x 2)))

(defn path-cost
  "Calculate the cost of the path so far plus the cheapest neighbor"
  [node-cost cheapest-neighbor]
  (+ node-cost
     (or (:cost cheapest-neighbor) 0)))

(defn total-cost
  "get the total cost of the path"
  [new-cost step-estimate size y x]
  (+ new-cost
     (estimate-cost step-estimate size y x)))

(defn min-by
  "retrieve the minimum value based on some criteria applied via a predicate fn"
  [f coll]
  (when (seq coll)
    (reduce (fn [min other]
              (if (> (f min) (f other))
                other
                min))
            coll)))

(defn neighbors
  "Find the neighbors of a cell in a 2D matrix"
  ([size yx]
   (neighbors [[-1 0] [1 0] [0 -1] [0 1]] size yx))
  ([deltas size yx]
   (filter

    ;; filter out illegal coordinates 
    (fn [new-yx] (every? #(< -1 % size) new-yx))

    ;; from all possible neighbor cell locations
    (map #(vec (map + yx %)) deltas))))

(defn a*
  "Find the best fit path given a set of costing heuristics"
  [start-yx step-estimate cell-costs]
  (let [size (count cell-costs)]
    (loop [steps     0

           ;; need a non-lazy sequence so not using threading macro
           routes    (vec (replicate size (vec (replicate size nil))))
           work-todo (sorted-set [0 start-yx])]
      (if (empty? work-todo)

        ;; grab the first route if our todo list is empty
        [(peek (peek routes)) :steps steps]

        ;; otherwise
        ;; 1. Get the next bit of work
        ;; 2. Clear that from the todo list
        ;; 3. Get the neighbors of this cell
        ;; 4. Get the cheapest neighbor
        ;; 5. Calculate the path costs so far
        ;; 5a. if the new path is worse, check the other paths (recur)
        ;; 5b. else add the route to our list
        (let [[_ yx :as work-item] (first work-todo)
              left-over-work       (disj work-todo work-item)
              neighbor-yxs         (neighbors size yx)
              cheapest-neighbor    (min-by :cost
                                           (keep #(get-in routes %) neighbor-yxs))
              new-cost             (path-cost (get-in cell-costs yx) cheapest-neighbor)
              old-cost             (:cost (get-in routes yx))]
          (if (and old-cost (>= new-cost old-cost))
            (recur (inc steps) routes left-over-work)
            (recur
             (inc steps)
             (assoc-in routes yx {:cost new-cost
                                  :yxs  (conj (:yxs cheapest-neighbor []) yx)})

                   ;; add the estimated path to the leftover work 
             (into left-over-work
                   (map
                    (fn [w]
                      (let [[y x] w]
                        [(total-cost new-cost step-estimate size y x) w]))
                    neighbor-yxs)))))))))
