(ns epi-clojure.rqm)

;; RMQ, the Range Minimum Query problem
;; Given two indexes into a vector, find the minimum value

(defn get-min [v]
   (loop [v v
          smallest Long/MAX_VALUE]
     (if (seq v)
       (recur (rest v) (min smallest (first v)))
       smallest)))

(defn rqm-super-naive
  "Just crunch along till we scan all indexes in the range and return the min found"
  [a, b, v]
  (get-min (subvec v a b)))

(def rqm-naive
  "This memoizes the fn so we're not super-naive ... but still O(n) every first access"
  (memoize rqm-super-naive))

(defn rqm-smartish
  "There exist only Theta(n^2) ways to slice a subarray, let's precompute them so we can do this in O(1)"
  [a, b, v]
  ;; First preprocess the vector
  )

(defn partition-vec 
  [v size]
  (let [len (count v)
        end-range (- len (- size 1))]
    (vec (for [x (range 0 end-range)
               :let [y (+ x size)
                     m (subvec v x y)]]
           (get-min m)))))

(defn build-partitions [v]
    (loop [n 1
         rqm-v (conj [] (partition-vec v 1))]
    (let [partition-size (Math/pow 2 n)
          pvec (partition-vec v partition-size)]
      (if-not (<= partition-size (count v))
        rqm-v
        (recur (inc n) (conj rqm-v pvec))))))

(defn precalculate-sparse-table
  [v]
     (loop [n 1
         rqm-v (conj [] (partition-vec v 1))]
    (let [partition-size (Math/pow 2 n)
          pvec (partition-vec v partition-size)]
      (if-not (<= partition-size (count v))
        rqm-v
        (recur (inc n) (conj rqm-v pvec))))))

(defn invert-table
  "Yes, I precalced the table backwards -- so I inverted it :)"
  [v]
  (loop [n 0
         table []]
    (if (>= n (count (first v)))
      table
      (let [row (into [] (flatten (map #(get % n) v)))]
        (recur (inc n) (conj table row))))))

(def invert (memoize invert-table))

;; memoize this so we get immutable precalc we can re-use.
(def sparse-table (memoize precalculate-sparse-table))

(defn rqm
  [a, b, v]
  ;; Find the largest k such that (2k ≤ j – i + 1), where K is a power of 2.
  ;; The range [i, j] can be formed as the overlap
  ;; of the ranges [i, i + 2k – 1] and [j – 2k  + 1, j].
;; ... seems to indicate I built the table in the wrong dimension :) 
;; ... inverting for now.
  (let [;; precalc the below tomorrow.
        k (last (for [i (range 0 (/ b 2))
                      :when [(<= (* 2 i) (inc (- b a)))]]
                  i))
        table (invert (sparse-table v))]
    (as-> a $
        (get table $)
        (get $ (- a  b)))))

