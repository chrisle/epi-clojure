(ns epi-clojure.algorithms.union-find
  (:refer-clojure :exclude [find union])
  (:require [taoensso.timbre :as timbre
             :refer [log  trace  debug  info  warn  error  fatal  report
                     logf tracef debugf infof warnf errorf fatalf reportf
                     spy get-env]]))

(defprotocol IQuickFind
  (qfind [this p q])
  (qunion [this p q]))

;; the node ids
(def ids (ref []))

;; component sizes
(def cz (ref []))

;; This seems efficient but is quadratic in time. 
;; MxN possible for M union commands on N objects
;; ... for all words in main memory (10^9), this is 300 compute years.
(defrecord QuickFind []
  IQuickFind
  (qfind [this p q] (= (get @ids p) (get @ids q)))
  (qunion [this p q]
    (dosync (ref-set ids (vec (let [p-id (get @ids p)
                                    q-id (get @ids q)]
                                (for [n @ids] (if (= n p-id) q-id n))))))))

(defn setup-quick-find [n]
  (dosync (ref-set ids (vec (range n)))))

(defprotocol IQuickUnion
  (ufind [_ p q])
  (uunion [_ p q])
  (uroot [_ i] "Root of i is [[[[i]]]] -- till you find the bottom turtle"))

;; Also slow :) 
;; Find is too expensive, and needs to do find to do a union (N steps)
(defrecord QuickUnion []
  IQuickUnion
  (ufind [_ p q] (= (uroot (->QuickUnion) p) (uroot (->QuickUnion) q)))
  (uroot [_ i]
    (let [parent (get @ids i)]
      (if (= parent i)
        i
        (recur parent))))
  (uunion [this p q]
    (dosync (alter ids assoc (uroot (->QuickUnion) p) q))))

(defn setup-quick-union [n]
  (dosync (ref-set ids (vec (range n))))
  (dosync (ref-set cz (into (vector-of :int) (repeat n 1)))))

(defprotocol IUnionFindStorageLayer
  (update-parent [this k v] "updates the parent of this element to a new value")
  (get-parent [this k] "gets the parent of this element in the set")
  (get-rank [this k] "gets the size of the set this element belongs too")
  (update-rank [this parent-root items] "updates the count of elements belonging to this set"))

(defprotocol IUnionFind
  (do-union [this u v items])
  (find* [this u v] "determine which subset a particular element is in and return the representative of that set")
  (make-set [this n] "create a disjoint set representing a collection")
  (rank [this i] "return the number of components in the set this element belongs to")
  (root [this i] "return the canonical root for this element")
  (union* [this u v] "merge two subsets into a single set with the representative of one set chose to lead them all"))

;; Avoids tall trees by merging smallest tree onto the largest, keeps track of the component sizes
;; uses a quick form of path compression (set each elems root to grandparent)
;; Find: lg N, Union: lg N ... which is effectively linear growth in steps wrt exponential growth of N.
(defrecord RankedUnionFind [storage-layer]
  IUnionFind
  (do-union [this new-parent new-child items]
    (info :params {:new-parent new-parent :new-child new-child :items items})
    (let [parent-root (root this new-parent)
          child-root (root this new-child)]
      (dosync
       (update-parent storage-layer child-root parent-root)
       (update-rank storage-layer parent-root items))))
  (find* [this p q] (= (root this p) (root this q)))
  (make-set [this n] (vec (range n)))
  (rank [_ i] (get-rank storage-layer i))
  (root [_ i]
    (info :params {:index i})
    (let [parent (get-parent storage-layer i)]
      (if (= parent i)
        i
        (do
          (update-parent storage-layer i (get-parent storage-layer parent))
          (recur parent)))))
  (union* [this p q]
    (let [p-count (rank this p)
          q-count (rank this q)]
      (if (<= p-count q-count)
        (do-union this q p p-count)
        (do-union this p q q-count)))))

(defn get-node-index [vertex coll]
  (keep-indexed #(when (= (get %2 1) vertex) %1) coll))

(defn get-parent-index [vertex coll]
  (keep-indexed #(when (= (get %2 1) vertex) (get %2 0)) coll))

;; Keeping this around as example since it's the simplest case that works
(defrecord OGStorageLayer []
  IUnionFindStorageLayer
  (update-parent [_ k v]
    (info :params {:k k :v v})
    (dosync (alter ids assoc k v)))
  (get-parent [_ k]
    (info :params {:k k})
    (get @ids k))
  (get-rank [_ k]
    (info :params {:k k})
    (get @cz k))
  (update-rank [_ parent-root items]
    (info :params {:root parent-root :items items})
    (ref-set cz (update @cz parent-root (partial + items)))))

(defrecord VertexStorageLayer [vertex->index-map index->vertex-map]
  IUnionFindStorageLayer
  (get-rank [_ k]
    (info :params {:key k})
    (get @cz (k vertex->index-map)))
  (get-parent [_ k]
    (info :params {:key k})
    (->> k
         vertex->index-map
         (get @ids)
         (get index->vertex-map)))
  (update-parent [_ k v]
    (info :params {:k k :val (k vertex->index-map) :v v :vval (v vertex->index-map)})
    (dosync (alter ids assoc (k vertex->index-map) (v vertex->index-map))))
  (update-rank [_ parent-root items]
    (info :params {:root parent-root :items items})
    (ref-set cz (update @cz (parent-root vertex->index-map) (partial + items)))))

(defn vertex->index-map
  "Create a set of [:node parent_index]"
  [coll]
  (apply merge-with conj (map-indexed #(assoc {} %2 %1) coll)))

(defn index->vertex-map
  "Create a set of [:node parent_index]"
  [coll]
  (apply merge-with conj (map-indexed #(assoc {} %1 %2) coll)))

(defn construct-union-find [n storage-layer]
  (let [union-find (->RankedUnionFind storage-layer)]
    (dosync (ref-set ids (make-set union-find n)))
    (dosync (ref-set cz (into (vector-of :int) (repeat (count @ids) 1))))
    union-find))
