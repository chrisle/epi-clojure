(ns epi-clojure.html-template)

;; HTML TEMPLATING ENGINE FOR INLINE TEXT SEMANTICS
;; 

;; samples to use with testing
(def sample-rules
  [{:start 0
    :end   7
    :tag   :a}
   {:start 0
    :end   6
    :tag   :p}
   {:start 0
    :end   6
    :tag   :p}
   {:start 1
    :end   4
    :tag   :b}
   {:start 2
    :end   6
    :tag   :u}])

;; common approach is to use two stacks and make a single pass through the string. *Instead* I chose 
;; to try to do a screen to world mapping. Which worked :) ... but is much more complex due to the
;; paritioning I used.


  ;; normalize rulesets passed in
  ;; they may be in the form of:
  ;;  { start: 1, end: 4, tag: 'b' },
  ;;  { start: 2, end: 6, tag: 'u' } ... which a rule crosses a boundary
  ;;  
  ;;  so ... do a merge of the rulesets, where we create a split as they cross a container boundary
  ;;  --> Start State
  ;;  { start: 0, end: 7, tag: 'a' } --> {<a> _ _ _ _ _ _ </a>}
  ;;  { start: 0, end: 6, tag: 'p' } --> {<p> _ _ _ _ _ </p> _}
  ;;  { start: 0, end: 6, tag: 'p' } --> {<p> _ _ _ _ _ </p> _}
  ;;  { start: 1, end: 4, tag: 'b' } --> {_ <b> _ _ </b> _ _ _}
  ;;  { start: 2, end: 6, tag: 'u' } --> {_ _ <u> _ _ _ </u> _}

 ;;
  ;; perform a reduction to boolean of the predicate over the collection item and it's neighbor
  ;; for all items in the collection: 
  ;;
  ;; example: 
  ;; (def coll [1 2 5 8 9 11 20 21 22])
  ;; (reductions not= true (map (fn [a b] (> (- b a) 2)) coll (rest coll)))
  ;; => (true true false true true true false false false)
  ;;
  ;; Then merge the boolean values with the original coll values and partition on the boolean: 
  ;; (partition-by second (map list coll switch))
  ;; => (((1 true) (2 true)) ((5 false)) ((8 true) (9 true) (11 true)) ((20 false) (21 false) (22 false)))
  ;;
  ;; and finally extract those values from each partition up to a list
  ;;
(defn partition-by-predicate
  "Partition a collection by a given predicate when the predicate changes value.
   Behaves like a group-by of the predicate return value."
  [pred? coll]
  (let [switch (reductions not= true (map pred? coll (rest coll)))]
    (map (partial map first) (partition-by second (map list coll switch)))))

(defn container-leaked?
  "is :a start <= :b start AND :a end < :b end"
  [a b]
  (and (<= (compare (:start a) (:start b)) 0)
       (< (compare (:end a) (:end b)) 0)))

  ;; screen to world
  ;; 
  ;;  --> Normalized Rule State (2n + s)
  ;;  { start: 0, end: 17, tag: 'a' } --> {<a> _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ </a>}
  ;;  { start: 1, end: 12, tag: 'p' } --> {_ <p> _ _ _ _ _ _ _ _ _ _ _ _ </p> _ _}
  ;;  { start: 2, end: 11, tag: 'p' } --> {_ _ <p> _ _ _ _ _ _ _ _ </p> _ _}
  ;;  { start: 3, end: 8, tag: 'b' }  --> {_ _ _ <b> _ _ _ _ </b>_ _ _ _ _}
  ;;  { start: 5, end: 7, tag: 'u' }  --> {_ _ _ _ <u> _ _ </u> _}
  ;;  { start: 9, end: 11, tag: 'u' } --> {_ _ <u> _ _ _ </u> _}

  ;; sort by start "time", add to coordinate plane
(defn adjust
  "split any containers that start in a container and cross the parent close boundary"
  [a b]
  (let [a-conflicted (last a)
        b-conflicted (first b)]
    (conj () a
          (conj (rest b) {:start (:start b-conflicted)
                          :end   (:end a-conflicted)
                          :tag   (:tag b-conflicted)}
                {:start (:end a-conflicted)
                 :end   (:end b-conflicted)
                 :tag   (:tag b-conflicted)}))))

(defn overlap->non-overlapped
  [ruleset]
  (let [partitions (partition-by-predicate container-leaked? ruleset)]
    (flatten (map #(adjust %1 %2) partitions (rest partitions)))))

(defn open-tag [tag]
  (str "<" (name tag) ">"))

(defn close-tag [tag]
  (str "</" (name tag) ">"))

(defn xform
  [input ruleset]
  (let [ruleset          (-> ruleset
                             overlap->non-overlapped)
        starts           (group-by :start (sort-by :end ruleset))
        ends             (group-by :end (sort-by :start ruleset))
        vectorized-input (map-indexed vector input)]
    (apply str
           (for [[k v] vectorized-input]
             (let [end-tags   (apply str (->> (reverse (get ends k))
                                              (map :tag)
                                              (map #(close-tag %))))
                   start-tags (apply str (->> (reverse (get starts k))
                                              (map :tag)
                                              (map #(open-tag %))))]
               (str end-tags start-tags v))))))