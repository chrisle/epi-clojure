(ns epi-clojure.language.macros)

(defmacro do-until 
  "Executes all clauses evaluating to true until one evaluates to false."
  [& clauses]
  (when clauses
    ;; when there are clauses
    (list 'clojure.core/when (first clauses)
          
          ;; build a list of each paired clause
          (if (next clauses)
            (second clauses)
            (throw (IllegalArgumentException. "do-until requires an even number of forms")))

          ;; recurse
          (cons 'do-until (nnext clauses)))))

(defmacro if-let*
  "Multiple binding version of if-let. Has the
   same behavior as a series of nested if-let's."
  ([bindings then]
   `(if-let* ~bindings ~then nil))
  ([bindings then else]
   (if (seq bindings)
     `(if-let [~(first bindings) ~(second bindings)]
        (if-let* ~(vec (drop 2 bindings)) ~then ~else)
        ~else)
     then)))

(defmacro defwatch
  "defines a watch function on a reference"
  [name & value]
  `(do
     (def ~name ~@value)
     (add-watch (var ~name)
                :re-bind
                (fn [~'key ~'r old# new#]
                  (println old# " -> " new#)))))

(defmacro with-resource 
  "An alternative to the with-open macro that 
  closes resources that are not auto closable"
  [binding close-fn & body]
  `(let ~binding (try
                    (do ~@body)
                    (finally 
                      (~close-fn ~(binding 0))))))

;; from CFtB&T
(defmacro enqueue 
  "Lets you name a promise, define how to derive the value to deliver 
to that promise, and define what to do with that promise. 
It also supports threading via the 4 arity version"
  ([q concurrent-promise-name concurrent serialized]
   `(let [~concurrent-promise-name (promise)]
      (future (deliver ~concurrent-promise-name ~concurrent))
      (deref ~q)
      ~serialized
      ~concurrent-promise-name))
  ([concurrent-promise-name concurrent serialized]
   `(enqueue (future) ~concurrent-promise-name ~concurrent ~serialized)))

;; defining a DSL using MACROS
;;
;;
;; (note: would probably do this with functions)
(defmacro domain 
 [name & body]
 `{:tag   :grouping
   :attrs {:name (str '~name)}
   :content [~@body]})

(declare handle-things)
(defmacro grouping 
  [name & body]
  `{:tag   :grouping
    :attrs {:name (str '~name)}
    :content [~@(handle-things body)]})

(declare grok-attrs grok-props)
(defn handle-things 
  [things]
  (for [t things]
    {:tag   :thing
     :attrs (grok-attrs (take-while (comp not vector?) t))
     :content (if-let [c (grok-props (drop-while (comp not vector?) t))]
                [c]
                [])}))

(defn grok-attrs
  [attrs]
  (into {:name (str (first attrs))}
         (for [a (rest attrs)]
           (cond
             (list? a) [:isa (str (second a))]
             (string? a) [:comment a]))))

(defn grok-props
  [props]
  {:tag   :properties 
   :attrs nil
   :content (apply vector (for [p props]
                            {:tag   :property 
                             :attrs {:name (str (first p))}
                             :content nil}))})

(def d
  (domain man-vs-monster
          (grouping people
                    (Human "A stock human")
                    (Man (isa Human)
                         "A man's man baby"
                         [name]
                         [has-beard?]))
          (grouping monsters
                    (Chupacabra
                     "a fierce elusive pumpkin patch demon"
                     [eats-goats?]))))

  
   
;; FEATURE FLAG MACROS (I developed these)
;;

(defmacro defflag
  "Creates a def for a key / value"
  [k v]
  `(def ~(symbol (name k)) ~v))

(defmacro defnpredicate
  "Creates a predicate for an existing defined var of the type '<var name>?'."
  [fname args & body]
  `(do (defn ~(symbol (str (name fname) "?")) ~args ~@body)))

(defmacro defnpredicates
  "Creates a set of predicates for the collection of symbols. The symbols must already be defined."
  [syms]
  `(do ~@(for [sym syms] `(defnpredicate ~(symbol (str (name sym) "?")) [] (true? ~sym)))))

(defmacro defflags
  "Creates defs for each k v pair in the input and then creates a predicate to test their state"
  [flags]
  `(do ~@(for [{:keys [flag value]} flags]
           (do
             `(defflag ~flag ~value)
             `(defnpredicate ~flag [] (true? ~value))))))
