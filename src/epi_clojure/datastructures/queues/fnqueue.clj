(ns epi-clojure.datastructures.queues.fnqueue
  "A simple functional queue (Purely Functional Data Structures pg 30)")

;; for a simple queue, record overhead would be significant
(defprotocol IQueue
  (balance [this f r])
  (emptyq? [this])
  (peekq [this])
  (pushq [this x])
  (popq [this])
  (tailq [this]))

(defrecord SimpleQueue [head tail]
  IQueue
  (balance [this f r] (if (empty? f)
                        (->SimpleQueue (reverse r) '())
                        (->SimpleQueue f r)))
  (emptyq? [this] (empty? head))
  (peekq [_] (peek head))
  (popq [this] (balance this (pop head) tail))
  (pushq [this x] (balance this head (cons x tail)))
  (tailq [this] (:tail (balance this head tail))))

(defprotocol ILazyQueue
  (lazy-balance [this f f-len r r-len])
  (lazy-emptyq? [this])
  (lazy-peekq [this])
  (lazy-pushq [this x])
  (lazy-popq [this])
  (lazy-tailq [this]))

(defrecord LazySimpleQueue [head head-len tail tail-len]
  ILazyQueue
  (lazy-balance [this f f-len r r-len] (if (empty? f)
                                    (->LazySimpleQueue (lazy-cat f (reverse r)) (+ head-len tail-len) '() 0)
                                    (->LazySimpleQueue f head-len r tail-len)))
  (lazy-emptyq? [this] (empty? head))
  (lazy-peekq [_] (first head))
  (lazy-popq [this] (lazy-balance this (rest head) (dec head-len) tail tail-len))
  (lazy-pushq [this x] (lazy-balance this head head-len (cons x tail) (inc tail-len)))
  (lazy-tailq [this] (:tail (lazy-balance this head head-len tail tail-len))))

;; alternative implementation using fns
;; http://blog.supplyframe.com/2013/06/11/implementing-persistent-queue-in-clojure/
(def balance-queue
  (memoize (fn [[f r :as this]]
             (if (empty? f) [(reverse r) '()] this))))
(defn q-head [[[x & _] _]] x)
(defn q-tail [[[_ & f] r]] (balance-queue [f r]))
(defn q-snoc [[f r] x] (balance-queue [f (cons x r)]))

;; lazy q - count realizes lazy sequences so keep track on our own
;; 
(def lazy-queue
  (memoize (fn [[f len-f r len-r :as q]]
             (println q)
             (if (<= len-r len-f) q
                 [(lazy-cat f (reverse r)) (+ len-f len-r) '() 0]))))
(defn lazy-snoc
  ([[f r :as q]] (lazy-snoc [f 0 r 0]))
  ([[f len-f r len-r :as q] x]
   (lazy-queue [f len-f (cons x r) (inc len-r)])))
(defn lazy-head [[f & _]] (first f))
(defn lazy-tail [[f len-f r len-r]]
  (lazy-queue [(rest f) (dec len-f) r len-r]))
