(ns epi-clojure.datastructures.heaps.skew)

;; https://themonadreader.files.wordpress.com/2010/05/issue16.pdf
(comment "Skew heaps were invented by Sleator and Tarjan [1], as a self-adjusting, but notably
unstructured, variety of heap. Skew heaps are binary trees, but unlike many
heap implementations, they don’t have any other constraints. Binary heaps, for
instance, are generally required to be completely balanced.
....          
          Theorem 14 (Sleator and Tarjan [1]) . The union operation on skew heaps takes
O (log n) amortized time. We won’t give the proof here, but intuitively, swapping the
children in this unusual fashion keeps the tree from getting excessively unbalanced.
For a particularly readable proof, consult the York University lecture notes [2] .")


(defprotocol ISkewHeap
  (extract-min [this])
  (insert [this p])
  (union [this p q]))

(defrecord SkewHeap []
  IPairingHeap
  (extract-min [_])
  (insert [_ p])
  (union [_ p q]))

