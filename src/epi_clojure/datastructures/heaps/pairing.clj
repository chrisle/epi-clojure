(ns epi-clojure.datastructures.heaps.pairing)

(defprotocol IPairingHeap
  (extract-min [this])
  (insert [this p])
  (union [this p q]))

(defrecord PairingHeap []
  IPairingHeap
  (extract-min [_])
  (insert [_ p])
  (union [_ p q]))
