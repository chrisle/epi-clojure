(ns epi-clojure.monads.common)

(defn ask [] identity)
(defn asks [f] 
  (fn [env] (f env)))

(def reader-m   
  {:return (fn [a] (fn [_] a))
   :bind   (fn [m k] (fn [r] ((k (m r)) r)))})  

(def maybe-m
  {:return (fn [v] v)
   :bind   (fn [mv f] (when mv (f mv)))})  

(def list-m 
  {:return (fn [v] (list v))
   :bind   (fn [mv f] (mapcat f mv))})  

(defn m-steps [m [name val & bindings] body]
  (if (seq bindings)
    `(-> ~val 
         ((:bind ~m) (fn [~name] ~(m-steps m bindings body))))
    `(-> ~val 
         ((:bind ~m) (fn [~name] ((:return ~m) ~body))))))

(defmacro do-monad
  [m bindings body]
  (m-steps m bindings body))

