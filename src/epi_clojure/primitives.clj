(ns epi-clojure.primitives
  (:gen-class)
  (:require 
   [clojure.spec.alpha :as s]

   ;; logging
   [taoensso.timbre :as timbre
    :refer [log  trace  debug  info  warn  error  fatal  report
            logf tracef debugf infof warnf errorf fatalf reportf
            spy get-env]])
)

;;
;; SPECS
;;
(s/def ::body ::cspec/significant-string)
(s/def ::status ::cspec/significant-string)

(s/fdef ::countBits
  :args (s/cat :value ::value)
  :ret ::topic-configuration)

(defn countBits
  "Counts the bits of a given value."
  [x]
  (println "Hello, World!"))
