(ns epi-clojure.vectors)

;; Given an array of integers, return indices of the two numbers such that they add up to a specific target.
;; You may assume that each input would have exactly one solution, and you may not use the same element twice.

;; Example:

;; Given nums = [2, 7, 11, 15], target = 9,

;; Because nums[0] + nums[1] = 2 + 7 = 9,
;; return [0, 1].

(defn getSumIndexes
  [nums target]

  ;; if this is huge, we'd want to filter <= target and use another array to hold the indexes
  ;; and operate on those 
  ;; 
  ;; a: vectorize the array so we have the indexes
  ;; b: use list comprehension and get the answers
    (vec (for [i (map-indexed vector nums)
               j nums :when (= (+ (second i) j) target)]
           (first i))))

;; interesting (and easy) permutation: print the numbers that sum to this out
;; 
(defn getNumbersThatSum [nums target]
  (for [a nums
        b nums :when (= (+ a b) target)] a))

;; another permutation, what if there are more than one. Can we return tuples?
;; Yes :) 
(defn getTupleSumIndexes
  [nums target]
  (vec (for [i (map-indexed vector nums)
             j (map-indexed vector nums) :when (= (+ (second i) (second j)) target)]
         [(first i) (first j)])))