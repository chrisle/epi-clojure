(ns epi-clojure.sorting.insertion-sort
  (:gen-class)
  (:require [clojure.test :as test]))

(defn insert-into
  "note - insert into a *sorted* collection"
  [coll item compare-fn]
  (let [colls (split-with (complement (partial compare-fn item)) coll)]
    (vec (concat (first colls) [item] (second colls)))))

;; currently horribly slow. 100- ~3.3ms, 1000 ~143ms, 10000 ~13560ms)
;; ...but it *is* a n^2 algorithm.
(defn insertion-sort
  [coll sort-fn]
  (reduce (fn [accum item]
            (if (not-empty accum)
              (let [result (sort-fn item (last accum))]
                (if (= -1 result)
                  (insert-into accum item <)
                  (conj accum item)))
              (conj accum item))) () coll))
