(ns epi-clojure.sorting.utils)

(defn check-results [results]
  (let [bools (map #(< % %2) results (rest results))
        first-false (some identity (map-indexed (fn [x i] (when (false? x) i)) bools))]
    (if (not (nil? first-false))
      (get results first-false)
      (println "Results OK"))))

(defn check-indexes [index original-vec]
  (let [bools (map #(< (get original-vec %) (get original-vec %2)) index (rest index))
        first-false (some identity (map-indexed (fn [x i] (when (false? x) i)) bools))]
    (if (not (nil? first-false))
      (get index first-false)
      (println "Results OK"))))

(defn rand-array [size]
  (vec (repeatedly size #(rand-int 10000000))))