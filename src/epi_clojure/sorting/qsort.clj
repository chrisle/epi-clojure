(ns epi-clojure.sorting.qsort)

;; step 1: if a the items are < N (some number), put them in the correct order and return
;; step 2: choose a pivot value, all cards less than this go on pile a, all items greater on pile b
;; recurse down on each pile
;; join left pile, pivot and right pile to get a sorted output
(defn qsort
  "sort a collection of items"
  [v compare-fn]
  (if (>= 1 (count v))
    v
    (let [{left 1 pivots 0 right -1} (group-by (partial compare-fn (peek v)) v)]
        ;;(println {:count (count v) :pivot-value (peek v) :left left :pivots pivots :right right})
      (concat (qsort left compare-fn) pivots (qsort right compare-fn)))))

;; run time is 5-6x slower than sort.

(defn qsort-index
  "sort an index into a collection of items"
  [v index compare-fn]
  (if (>= 1 (count index))
    index
    (let [{left 1 pivots 0 right -1} (group-by (partial #(compare (get v (peek index)) (get v %))) index)]
        ;; (println {:count (count index) :pivot-value (peek v) :left left :pivots pivots :right right})
      (vec (lazy-cat (qsort-index v left compare-fn) pivots (qsort-index v right compare-fn))))))

(defn create-index
  [v]
  (vec (range 0 (count v))))

(defn sort-parts
  "lazy,tail recursive, incremental quicksort from Joy of Clojure. Likely not good perf :)"
  [work]
  (lazy-seq
   (loop [[part & parts] work]
     (if-let [[pivot & xs] (seq part)]
       (let [smaller?     #(< % pivot)
             working-list (list*
                           (filter smaller? xs)
                           pivot
                           (remove smaller? xs)
                           parts)]
         (recur working-list))
       (when-let [[x & parts] parts]
         (cons x (sort-parts parts)))))))

(defn joy-qsort
  [xs]
  (sort-parts (list xs)))
