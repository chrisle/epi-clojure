(ns epi-clojure.sorting.merge-sort)

;; MergeSort(arr[], l,  r)
;; If r > l
;;      1. Find the middle point to divide the array into two halves:  
;;              middle m = (l+r)/2
;;      2. Call mergeSort for first half:   
;;              Call mergeSort(arr, l, m)
;;      3. Call mergeSort for second half:
;;              Call mergeSort(arr, m+1, r)
;;      4. Merge the two halves sorted in step 2 and 3:
;;              Call merge(arr, l, m, r)
;;  

;; use lazy-seq to avoid overflow
(defn merge*
  [p q]
  (let [p* (first p)
        p-> (rest p)
        q* (first q)
        q-> (rest q)]
    (lazy-seq (cond
                (not p*) q
                (not q*) p
                (< p* q*) (cons p* (merge* p-> q))
                :else (cons q* (merge* q-> p))))))

;; runtime: ~40ms for 10K, vs 20ms for sort.
(defn mergesort
  ([coll] (mergesort coll merge*))
  ([coll merge-fn]
   (let [idx (/ (count coll) 2)
         left  (subvec coll 0 idx)
         right (subvec coll idx)]
    ;; (println {:partition idx :left left :right right})
     (if (<= (count coll) 1)
       coll
       (merge-fn (mergesort left merge-fn)
                 (mergesort right merge-fn))))))

(defn msort-merge
  "sort an index into a collection of items using mergesort"
  [arr compare-fn p q]
  (let [p*  (first p)
        p-> (rest p)
        q* (first q)
        q-> (rest q)]
    (lazy-seq (cond
                (not p*) q
                (not q*) p
                (compare-fn (get arr p*) (get arr q*)) (cons p* (msort-merge arr compare-fn p-> q))
                :else (cons q* (msort-merge arr compare-fn q-> p))))))

(defn msort-index
  [coll index compare-fn]
  (let [idx (/ (count index) 2)
        left  (subvec index 0 idx)
        right (subvec index idx)]
    ;; (println {:partition idx :left left :right right})
    (if (<= (count index) 1)
      index
      (msort-merge coll
                   compare-fn
                   (msort-index coll left compare-fn )
                   (msort-index coll right compare-fn)))))

(defn create-index
  [v]
  (vec (range 0 (count v))))