(ns epi-clojure.graphs.digraph
  (:require [clojure.walk :as walk :refer [postwalk]]
            [taoensso.timbre :as timbre
             :refer [log  trace  debug  info  warn  error  fatal  report
                     logf tracef debugf infof warnf errorf fatalf reportf
                     spy get-env]]))

;; digraph
{:a [:b :c]
 :b [:a :c :d]
 :c [:e :f]}

;; tree (rooted acyclic digraph, aka rooted tree)
{:root [:a :b]
 :a [:c :d]
 :b [:e :f]
 :c []
 :d [:g :h]
 :e [:i]
 :f [:j :k]
 :g []
 :h []
 :j []
 :k []}

;; Vectors are Clojures basic data type (besides lists) and can have heterogenous items
;; if you are dealing with templating, strings, vectors and lists are good
{:root [:a [:c :d [:g :h]] :b [:e [:i] :f [:j :k]]]}

;;or
{:root '(:a '(:c :d '(:g :h)) :b '(:e '(:i)) :f '(:j :k))}

;; associative stuctures have O(1) access by key. So for homogenous
;; collections of nodes where you constrain the tree inputs things 
;; like maps or records are good.

;;or 
;;
{:root
 {:a {:c {}
      :d {:g {}
          :h {}}}
  :b {:e {:i {}}
      :f {:j {}
          :k {}}}}}

;; or represented as records (for constrained trees like BinaryTrees)

;; however our chosen representation will be a combination
;; a. access nodes by hash
;; b. define edges as a key to the node and some attributes (or edge structures)
;; c. data in the node map
;; 
;; Note- cannot have hyperedges, where one node edge connects to multiple other nodes.
{:root {:vertices {:a 10 :b 5} :data []}
 :a {:vertices {:c 10 :d 5} :data []}
 :b {:vertices {:e 10 :f 5} :data []}
 :c {:vertices {} :data []}
 :d {:vertices {:g 20 :h 5} :data []}
 :e {:vertices {:j 10} :data []}
 :f {:vertices {:j 10 :k 10} :data []}
 :g {}
 :h {}
 :j {}
 :k {}}

;; store data in another hashmap by node key
{:root []
 :a []}

(def acyclic-graph {:nodes {:root {:a 10 :b 5}
                            :a {:c 10 :d 5}
                            :b {:e 10 :f 5}
                            :c {}
                            :d {:g 20 :h 5}
                            :e {:j 10}
                            :f {}
                            :g {}
                            :h {}}
                    :acyclic true})

(def graph-with-cycles {:nodes {:root {:a 10 :b 5}
                                :a    {:c 10 :d 5}
                                :b    {:e 10 :f 5}
                                :c    {:g 4}
                                :d    {:g 20 :h 5}
                                :e    {:j 10 :e 20}
                                :f    {:b 10}
                                :g    {:e 5}
                                :h    {:f 15}
                                :j    {}}
                        :acyclic false})

(def DEFAULT_WEIGHT 100)
(defn digraph
  ([] (digraph false))
  ([acyclic]
   {:pre [(boolean? acyclic)]}
   {:acyclic? acyclic})
  ([acyclic tree-property]
   {:pre [(boolean? acyclic)
          (keyword? tree-property)]}
   {:acyclic? acyclic
    :property tree-property}))

(def edge-data (atom {}))

(defn create-edge
  ([key]
   (key nil nil))
  ([key vertices]
   {(keyword key)
    (reduce (fn [accum next] (conj accum {(keyword next) DEFAULT_WEIGHT})) {} vertices)}))

(defn neighbors [graph vertex]
  (keys (vertex (:nodes graph))))

(defn visited? [key coll]
  (some #{key} coll))

(defn get-cycles
  "Traverses a graph based on passed in stack"
  [graph start]
  (loop [cycles '()
         stack   (list start)      ;; Use a the provided stack to store nodes we need to explore
         visited []]        ;; A vector to store the sequence of visited nodes
    (if (empty? stack)      ;; Base case - return visited nodes if the stack is empty
      cycles
      (let [v           (peek stack)
            neighbors   (neighbors graph v)
            new-cycles (filter #(visited? % visited) neighbors)
            not-visited (filter (complement #(visited? % visited)) neighbors)
            new-stack   (into (pop stack) neighbors)
            stack (into (pop stack) not-visited)]
        (trace :visiting {:cycles cycles :new-cycles new-cycles  :node v :visited visited :neighbors neighbors :new-stack (seq new-stack)})
        (cond
          (seq new-cycles) (recur (conj cycles {:start v :cycles new-cycles}) stack (conj visited v))
          :else (recur cycles new-stack (conj visited v)))))))

(defn has-cycle?
  "Traverses a graph based on passed in stack"
  [graph start]
  (loop [stack   (list start)      ;; Use a list so we do a DFS
         visited []]        ;; A vector to store the sequence of visited nodes
    (if (empty? stack)      ;; Base case - return false if we process all nodes only once
      false
      (let [v           (peek stack)
            neighbors   (neighbors graph v)
            new-stack   (into (pop stack) neighbors)]
        (cond
          (visited? v visited) true  ;; we visited a node twice, we have a cycle folks.
          :else (recur new-stack (conj visited v)))))))

(defn add-edge
  ([graph vertex edge]
   (add-edge graph vertex edge DEFAULT_WEIGHT))
  ([graph vertex edge weight]
   {:pre [(if (:acyclic graph)
            (not (has-cycle? (update-in graph [:nodes vertex] assoc edge weight)
                             :root))
            true)]}
   (update-in graph [:nodes vertex] assoc edge weight)))

(defn add-vertex [graph vertex]
  {:pre [(if (:acyclic graph)
           (not (has-cycle? (update-in graph [:nodes vertex] assoc {})
                            :root))
           true)]}
  (update-in graph [:nodes] assoc vertex {}))

(defn remove-edge-to-vertex [graph vertex]
  (map #(if-not (contains? (second %) vertex)
          (println %)
          (dissoc (second %) vertex)) graph))

(defn remove-vertex
  "Remove the vertex and all associated edges"
  [graph vertex]
  (let [vertex-key (first (keys vertex))]
    (->> vertex-key
         (dissoc graph)
         (postwalk #(if (vertex-key %) (dissoc % vertex-key) %)))))

(defn weight-edge [graph vertex edge weight]
  (update-in graph [:nodes vertex edge] assoc weight))

(defn remove-vertex [graph edge vertex]
  (update-in graph [:nodes edge] dissoc vertex))

(defn get-outgoing-edges [graph vertex]
  (keys (vertex (:nodes graph))))

(defn get-incoming-edges [graph vertex]
  (reduce (fn [accum next]
            (if (contains? (second next) vertex)
              (conj accum (first next))
              accum))
          []
          (:nodes graph)))

(defn create-data-table
  "get a data store for our graph"
  [graph]
  (reduce #(assoc % (first %2) []) {} (:nodes graph)))

;; TBD -- create a double ended queue
(defn queue
  ([] (clojure.lang.PersistentQueue/EMPTY))
  ([coll]
   (reduce conj clojure.lang.PersistentQueue/EMPTY coll)))
(defn search
  "given a collection node(s) - search until we find it or return nil"
  [graph found? non-visited-coll]
  (loop [visited '()
         node-stack non-visited-coll]
    ;; (info {:not-visited (seq node-stack) :visited visited})
    (when (seq node-stack)
      (let [node-key (peek node-stack)]
        (if-not (some #{node-key} visited)
          (let [neighbors (neighbors graph node-key)
                newly-visited (conj visited node-key)
                not-visited (pop node-stack)]
            (info :visiting {:node-key node-key})
            (cond
              (found? node-key) (do (info "found!" node-key) node-key)
              (empty? neighbors) (recur newly-visited not-visited)
              :else (recur newly-visited (apply conj not-visited neighbors))))
          (recur visited node-stack))))))

;; This comes from: https://dnaeon.github.io/graphs-and-clojure/ as a comparison to the one I wrote above.
;; Peformance is equivalent, the one below feels more visually appealing because of reduced branching on the left.
;; This moves that to the right.
(defn graph-search
  "Traverses a graph based on passed in stack"
  [graph start found? stack]
  (loop [stack   (conj stack start)      ;; Use a the provided stack to store nodes we need to explore
         visited []]        ;; A vector to store the sequence of visited nodes
    (if (empty? stack)      ;; Base case - return visited nodes if the stack is empty
      visited
      (let [v           (peek stack)
            neighbors   (neighbors graph v)
            not-visited (filter (complement #(visited? % visited)) neighbors)
            new-stack   (into (pop stack) not-visited)]
        (info :visiting {:node v :neighbors neighbors :new-stack (seq new-stack)})
        (cond
          (found? (v graph)) (do (info :found {:target v}) v)
          (visited? v visited) (recur new-stack visited)
          :else (recur new-stack (conj visited v)))))))

(defn graph->edges
  "Returns the graph as an adjancency list of spans
   e.g.: A -> B -> C --> [[A, B][B, C]]"
  [graph]
  (loop [stack (list :root)
         visited []
         adjacency-list []]
    (if (empty? stack)
      adjacency-list
      (let [v (peek stack)
            neighbors (neighbors graph v)
            not-visited (filter (complement #(visited? % visited)) neighbors)
            new-stack (into (pop stack) not-visited)
            new-alist (into adjacency-list (map #(conj {:from v :to % :weight (% (v (:nodes graph)))}) not-visited))]
        (info :visiting {:node v :neighbors neighbors :new-stack (seq new-stack)})
        (cond
          (visited? v visited) (recur new-stack visited new-alist)
          :else (recur new-stack (conj visited v) new-alist))))))

(defn dfs [graph vertex found?]
  (graph-search graph vertex found? '()))

(defn bfs [graph vertex found?]
  (graph-search graph vertex found? (queue)))

(defn get-depth [graph edge])

(defn zigzag-search
  "Does a breadth search but flips the queue every level"
  [graph start found?]
  (loop [stack   (conj (queue) start)      ;; Use a the provided stack to store nodes we need to explore
         visited []]                       ;; A vector to store the sequence of visited nodes
    (if (empty? stack)      ;; Base case - return visited nodes if the stack is empty
      visited
      (let [v           (peek stack)
            neighbors   (neighbors graph v)
            not-visited (filter (complement #(visited? % visited)) neighbors)
            new-stack   (as-> stack $
                          (pop $)
                          (if (odd? (get-depth v graph))
                            (into $ (reverse not-visited))
                            (into $ not-visited)))]
        (info :visiting {:node v :neighbors neighbors :new-stack (seq new-stack)})
        (cond
          (found? v) (do (info :found {:target v}) v)
          (visited? v visited) (recur new-stack visited)
          :else (recur new-stack (conj visited v)))))))

(defn binary-property-holds? [vertex]
  (< 2 (count (keys vertex))))

(defn heap-property-holds? [parent child])

(defn add-respects-tree-property? [graph compare-fn parent-key child-key]
  (cond (:property graph)
        :binary (binary-property-holds? (parent-key graph))
        :heap (compare-fn (parent-key graph) (child-key graph))
        :search (compare-fn (parent-key graph) (child-key graph))))

;; TBD fns to implement on graph operations.
(defn contract [g key-a key-b])

(defn complement [g])

(defn transpose [g])

(defn line-graph [g])

(defn edit-distance [a b])
