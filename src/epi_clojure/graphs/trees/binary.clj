(ns epi-clojure.graphs.trees.binary
  (:require [epi-clojure.graphs.digraph :as digraph]))

(defrecord TreeNode [parent left right data])

(defn btree 
  "returns an empty tree"
  []
  (digraph/digraph true :binary))

(defn add-child [tree child]
    ;; find a node to insert at
  (let [parent  (digraph/dfs tree (fn [key] (<= 2 (count (key tree)))))]
    (-> tree
        (digraph/add-vertex tree child)
        (digraph/add-edge parent child))))

